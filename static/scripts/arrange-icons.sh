#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

# Every second rearrange the desktop icons if the screen resolution has changed
xfdesktop --arrange
prev_screen_res=""
while true; do
  curr_screen_res="$(xrandr | grep '*')"
  if [ "$prev_screen_res" != "$curr_screen_res" ]; then
    xfdesktop --arrange
    prev_screen_res="$curr_screen_res"
  fi
  sleep 1;
done
