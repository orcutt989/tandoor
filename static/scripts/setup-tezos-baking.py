#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

import os
import time
from subprocess import run, check_output, check_call, call

# This launches the setup wizard if there isn't a previous setup already

try:
    # we first look for any node to be running (either alone or with baking)
    node_state = run("sudo systemctl is-active tezos-node-*.service", capture_output=True, shell=True)
    node_is_active = node_state.returncode == 0
    # sometimes (especially with tezos-baking services) a node unit is still in
    # the process of being activated at this point
    node_is_activating = "activating" in node_state.stdout.decode().splitlines()
    # if there is an active or activating node, the setup wizard isn't launched
    if node_is_active or node_is_activating:
        exit()
    # otherwise a maximize terminal will be launched
    check_call('xfce4-terminal --maximize -e "return_to_exit tezos-setup"', shell=True)

except Exception as e:
    print(e)
