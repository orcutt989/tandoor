<!--
SPDX-FileCopyrightText: 2022-2023 Oxhead Alpha
SPDX-License-Identifier: LicenseRef-MIT-OA
-->

# Tandoor

**Disclaimer: Currently Tandoor is in an alpha-level state and is not ready for production usage. Avoid using it for baking on mainnet.**

Tandoor is a virtual appliance for baking on Tezos.

It aims to be contained, with batteries included, easy to set up and to tear down.

![desktop](docs/screenshots/desktop.png "Default desktop")

## Installation and setup

### Prerequisites

Tandoor needs a virtual machine to run into and VirtualBox is the easiest way to
set one up for it.

__Note: other hypervisors should work as well, but compatibility is limited and
some features may be unsupported.__

You'll need to install:
1. [VirtualBox](https://www.virtualbox.org/), version `7.0.0` or newer
2. the [Extension Pack](https://www.virtualbox.org/manual/ch01.html#intro-installing)

Lastly, if you're using Linux, we also recommend adding your user to the
`vboxusers` group, by running the command:
```sh
sudo usermod -a -G vboxusers $USER
```
This will allow Tandoor to access USB devices, such as [Ledger](https://www.ledger.com/)'s.

On MacOS, to access USB devices VirtualBox must run as root (see https://www.virtualbox.org/ticket/21218),
start it with the following command:

```sh
sudo VirtualBox
```

### Getting and importing the appliance

Tandoor comes as an [OVA](https://en.wikipedia.org/wiki/Open_Virtualization_Format) images, download the latest from [Releases](https://gitlab.com/tezos-kiln/tandoor/-/releases/)

<!-- TODO: there is no URL for the latest package AFAICT, but perhaps this could be automated, or some screenshots could be added here too. -->

At this point, importing the image should be as simple as opening the
`tandoor.ova` file that you just downloaded.

Tandoor should now appear in VirtualBox's UI, from which you can start and manage
your VM:

![virtualbox list](docs/screenshots/vbox-list.png "VBox list UI")

### Baking setup

When running Tandoor for the first time, you'll find an interactive wizard
waiting for you. It will ask you some questions (e.g. which network to use, how to bootstrap node, etc) and set up your baking instance.

![setup](docs/screenshots/setup.png "First setup")

In the end, all the necessary Octez software will be running, as well as [pyrometer](https://gitlab.com/tezos-kiln/pyrometer)
for monitoring.

NOTE: `pyrometer` will work properly out of the box for setups using the default client and node data directories.
If different directories are chosen, then a manual restart of pyrometer is needed.

You can use the shortcuts on the desktop to run the wizard again, if you'd like
to re-configure your setup, or to open some of the other utilities included:

![monitor](docs/screenshots/monitor.png "VM monitoring")

## Updates and maintenance

Tandoor will attempt to keep its software up-to-date, by:
1. checking for new `pyrometer` releases regularly and updating when possible
2. checking for new `tezos-packaging` packages regularly and showing in the top
   bar when these are available

Alternatively, one can check for these manually by running in a terminal:
```bash
sudo install-latest-pyrometer
```
and
```bash
sudo apt update -y; sudo apt-get upgrade
```
respectively.

It's also possible to check for `tezos-packaging` manually by launching the
"Package Updater" from the icon on the desktop.

Note that these should be sufficient for Tandoor to keep a working baking setup,
but won't update Tandoor itself, which might be different in new releases.

We recommend users to consider their Tandoor VMs as disposable and to set up a
new one with the latest release periodically (or in case of issues).

## Building and installing from source

If you don't want to install Tandoor from the pre-built OVA you can also build
and install it from source.

### Prerequisites

* [Packer](https://www.packer.io/)
* [VirtualBox](https://www.virtualbox.org/) version `7.0.0` or newer
* [Extension Pack](https://www.virtualbox.org/manual/ch01.html#intro-installing)
* [curl](https://curl.se/)
* [XCode Command Line Tools](https://mac.install.guide/commandlinetools/4.html) (for macOS only)

### Building and installing

You can build and import Tandoor into Virtualbox at once with one command:
```sh
make install
```
or you can run `make` if you only want to build the OVA image.

By default, these will download a prebuilt `ova` with a fresh `debian` install,
to dramatically decrease build time.
However, if you want to force the rebuild of this first stage of Tandoor, you can
set the environment variable:
``` sh
REBUILD_FIRST_STAGE=true
```

If you instead want to override the download link for the base image,
set the environment variable:

``` sh
TANDOOR_BASE_URL=<url>
```

All the resulting images will be located under the `artifacts` directory.

This VM builder has configurable variables, which you can set by editing the
`tandoor-base.pkrvars.hcl` file, to specify the local path to the
base image, its checksum, the VM's RAM, its disk size, the amount of vCPUs, etc.
