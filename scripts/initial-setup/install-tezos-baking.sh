#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

apt install -y software-properties-common

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 37B8819B7D0D183812DCA9A8CE5A4D8933AE7CBB
add-apt-repository -yu 'deb http://ppa.launchpad.net/serokell/tezos/ubuntu focal main'

apt install -y tezos-baking
