#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

rm -rf /lib/firmware/*
rm -rf /usr/share/doc/linux-firmware/*

rm -rf /usr/share/doc/*

find /var/cache -type f -exec rm -rf {} \;

find /var/log -type f -exec truncate --size=0 {} \;

rm -rf /tmp/* /var/tmp/*

apt-get --assume-yes purge cloud-init
rm -rf /etc/cloud/ && sudo rm -rf /var/lib/cloud/

rm -rf /home/tandoor/VBoxGuestAdditions.iso

apt-get --assume-yes autoremove
apt-get --assume-yes clean

# Zero out the free space to save space in the final image
dd if=/dev/zero of=/EMPTY bs=1M || true
rm -f /EMPTY
sync
