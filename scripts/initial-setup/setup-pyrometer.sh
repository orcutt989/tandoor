#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

cat > /etc/pyrometer.toml <<EOF
data_dir = "/root/.local/share/pyrometer"
exclude = [ "baked", "endorsed" ]

[baker_monitor]
max_catchup_blocks = 120
head_distance = 2
missed_threshold = 5
rpc = "http://127.0.0.1:8732"

[log]
level = "info"
timestamp = false

[node_monitor]
nodes = [ "http://127.0.0.1:8732" ]
teztnets = false
teztnets_config = "https://teztnets.xyz/teztnets.json"
low_peer_count = 5

[notifications]
interval = 60
max_batch_size = 100
ttl = 86_400

[ui]
enabled = true
host = "localhost"
port = 2_020
explorer_url = "https://tzstats.com"

[rpc]
retry_attempts = 3
retry_interval_ms = 1_000
EOF

# path unit template which watches tezos-node's configuration files
# and triggers pyrometer restart on changes
cat <<EOF > /etc/systemd/system/pyrometer-restarter_tezos-node@.path
[Path]
PathModified=/var/lib/tezos/node-%i/config.json
Unit=pyrometer-restarter.service

[Install]
WantedBy=multi-user.target
EOF

# path unit which watches tezos-client's configuration files changes
# and triggers pyrometer restart on changes
cat <<EOF > /etc/systemd/system/pyrometer-restarter_tezos-client.path
[Path]
PathModified=/var/lib/tezos/.tezos-client/config
PathModified=/var/lib/tezos/.tezos-client/public_key_hashs
Unit=pyrometer-restarter.service

[Install]
WantedBy=multi-user.target
EOF

# oneshot systemd service which restarts pyrometer.service
cat <<EOF > /etc/systemd/system/pyrometer-restarter.service
[Unit]
Description=Pyrometer restarter

[Service]
Type=oneshot
ExecStart=/usr/bin/systemctl restart pyrometer.service

[Install]
WantedBy=multi-user.target
EOF

# pyrometer service should have access to some tezos configuration files
SYSTEMD_EDITOR=tee systemctl edit pyrometer.service <<EOF
[Service]
User=tezos
EOF

systemctl daemon-reload
for path in /var/lib/tezos/node-*
do
  i="$(basename "$path" | cut -d'-' -f2)"
  systemctl enable "pyrometer-restarter_tezos-node@$i.path"
done
systemctl enable pyrometer-restarter_tezos-client.path
