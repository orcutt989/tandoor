#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

apt install -y git

mkdir /opt/tandoor
git clone https://gitlab.com/tezos-kiln/tandoor.git /opt/tandoor --branch "$TANDOOR_BRANCH" --single-branch

cat <<EOT >> /home/tandoor/.bashrc
if baking_service="\$(set -o pipefail && systemctl list-units --full --type=service --state=active | cut -d' ' -f3 | grep -E "tezos-baking|tezos-node" | sort | head -n 1)"; then
    echo "Currently tandoor is running '\$baking_service'"
fi
EOT

cat <<EOT >> /usr/bin/return_to_exit
#!/usr/bin/env bash
# Commodity script to avoid closing terminals after the execution of a command
"\$@"
echo ""
read -p "Press Return to close this window"
EOT
chmod +x /usr/bin/return_to_exit

ln -sf /opt/tandoor/scripts/maintenance/install-latest-pyrometer.sh /usr/local/bin/install-latest-pyrometer
