#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

set -euo pipefail

listvms () {
    VBoxManage list "${1:-""}vms" | grep tandoor | cut -d\" -f2
}

for vm in $(listvms running); do VBoxManage controlvm "$vm" poweroff; done

for vm in $(listvms); do while ! VBoxManage unregistervm "$vm" --delete 2> /dev/null; do sleep 1; done; done
