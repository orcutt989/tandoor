#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2022 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

set -euo pipefail

packer init tandoor.pkr.hcl

# due to `set -e`, script will be stopped in case of build error
# however, `make` will recognize it as successfully built in case of error
# so we touch its' dep before build
touch tandoor.pkr.hcl

packer build -force -var-file tandoor.pkrvars.hcl tandoor.pkr.hcl

# dummy file we use to witness that tandoor was already built
# it's unobvious how to specify exact file to be build as Make target
# due to naming policy
touch artifacts/.tandoor
