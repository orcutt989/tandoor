#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Oxhead Alpha
# SPDX-License-Identifier: LicenseRef-MIT-OA

apt install -y jq

latest_release="https://gitlab.com/api/v4/projects/22897259/releases/permalink/latest"
deb_package_link="$(curl -s -L "$latest_release" | jq -r '.assets.links | .[] | select(.name == "Ubuntu/Debian") | .direct_asset_url')"
wget -O pyrometer.deb "$deb_package_link"
apt install ./pyrometer.deb
rm pyrometer.deb
