## Description

<!--
Describes the nature of your changes. If they are substantial, you should
further subdivide this into a section describing the problem you are solving and
another describing your solution.
-->

## Related issue(s)

<!--
- Short description of how the MR relates to the issue, including an issue link.
For example
- Fixed #999 by adding lenses to exported items

Write 'None' if there are no related issues (which is discouraged).
-->

Resolves #

## :white_check_mark: Checklist for your Merge Request

<!--
Ideally a MR has all of the checkmarks set.

If something in this list is irrelevant to your MR, you should still set this
checkmark indicating that you are sure it is dealt with (be that by irrelevance).

If you don't set a checkmark (e. g. don't add a test for new functionality),
you must be able to justify that.
-->

#### Related changes (conditional)

- [ ] I checked whether I should update [README](/README.md).
- [ ] I checked whether virtual appliance building works.

#### Stylistic guide (mandatory)

- [ ] My commits comply with [the following policy](https://www.notion.so/serokell/Where-and-how-to-commit-your-work-58f8973a4b3142c8abbd2e6fd5b3a08e).
